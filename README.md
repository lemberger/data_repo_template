<!--
This file is part of the repository template for
data analyses of benchexec-based evaluations:
https://gitlab.com/lemberger/data_repo_template

SPDX-FileCopyrightText: 2020 Dirk Beyer <https://sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

# Template for Data-Analyses Repositories

[![Apache 2.0 License](https://img.shields.io/badge/license-Apache--2-brightgreen.svg?style=flat)](https://www.apache.org/licenses/LICENSE-2.0)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/ADDREPOURL/master?filepath=Analysis.ipynb)

## First steps

1. Adjust license information:
    * Adjust `.reuse/templates/header.jinja2`
    * Check and adjust `.reuse/dep5` - this file puts whole directories under a certain license.
    * Check that `LICENSE` is the correct license and put your licenses in folder `LICENSES`


## Usage

### Licensing new files

You can easily add license headers to new files with [`reuse`](https://reuse.software/):

```bash
reuse addheader --template header --copyright "PERSON <WEBPAGE>" --year YEAR --license YOUR_LICENSE FILE(S)
```

You can run `reuse lint` to check that all your repository files are licensed.


### Running the Jupyter Notebook

To install requirements, you can run `pip install -r requirements.txt`.

To run the notebook, run `jupyter notebook` or `jupyter lab` in the root directory of this repository,
and open notebook `Analysis.ipynb` in the file explorer of the notebook interface.

## License

This project is licensed under the Apache 2.0 License with copyright by Dirk Beyer (2020).