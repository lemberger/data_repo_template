<!--
This file is part of the repository template for
data analyses of benchexec-based evaluations:
https://gitlab.com/lemberger/data_repo_template

SPDX-FileCopyrightText: 2020 Dirk Beyer <https://sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

This directory contains all raw data (benchexec result files, logfiles, etc.)